#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>

#include "processos.h"


int fcfs_debug = 0;

void* process_job(void* _p) {
    Proc* proc = (Proc*) _p;
    double a, b;

    if (fcfs_debug) {
        int my_cpu = sched_getcpu();
        fprintf(stderr, "Processo: %s pegou a cpu %d\n", proc->nome, my_cpu);
    }

    a = 23;
    b =12;
    while (1) {
        pthread_mutex_lock(&lockers->mu[proc->id]);
        b = (a+b*b*a/b*a)*b*b*b*b;
        pthread_mutex_unlock(&lockers->mu[proc->id]);
    }
    return NULL;
}



void* fifo_scheduling(FILE* output_f, int debug) {
    double start, now;
    double time_alive = 0; 
    unsigned int trocas_contexto = 0;
    start = get_time();

    if (debug) {
        fprintf(stderr, "Iniciei escalonador fcfs!!\n");
    }

    Proc* cur = NULL;
    Proc* cur_fila;
    Proc* next_proc;

    while (1) {
        next_proc = NULL;

        if (cur) {
            double _now = get_time();
            cur->remaining -= (_now - cur->start)/1000000;//em segundos
            cur->start = _now;

            if (cur->remaining <= 0) {
                if (debug) {
                    fprintf(stderr, "---- Acabei de executar o processo %s pid: %d, time_alive: %f\n", cur->nome, cur->id, time_alive);
                }
                pthread_mutex_lock(&lockers->mu[cur->id]);
                pthread_cancel(lockers->tids[cur->id]);

                // Atualiza o tempo do relogio exatamente
                now = get_time();
                time_alive = (now - start)/1000000;
                fprintf(output_f, "%s %f %f\n", cur->nome, time_alive, time_alive - cur->t0);

                // tiramos o next proc da fila
                if (cur->prev) {
                    cur->prev->next = cur->next;    
                }
                if (cur->next) {
                    cur->next->prev = cur->prev;
                }

                if (fila->head == cur) {
                    fila->head = fila->head->next;
                }

                cur = NULL; //Muito cuidado!
                fila->size--;
                if (debug) {
                    fprintf(stderr, "Tamanho da fila %d \n", fila->size);
                    fprintf(stderr, "\n");
                }
            }

            if (fila->size == 0) {
                if (debug) {
                    fprintf(stderr, "Acabei o fcfs!!!\n");
                }
                break;
            }
        }

        // Atualizar os status dos processos
        cur_fila = fila->head;

        while (cur_fila) {
            if (!cur_fila->enabled && time_alive >= cur_fila->t0) {
                cur_fila->enabled = 1;
            }

            if (cur_fila->enabled) {
                next_proc = cur_fila;
                break;
            }
            else {
                cur_fila = cur_fila->next;
            }
        }

        if (!cur && next_proc) {
            cur = next_proc;
            cur->id = lockers->size;
            lockers->size++;
            cur->start = get_time();
            pthread_create(&lockers->tids[cur->id], NULL, process_job, (void *)cur);
            if (debug) {
                fprintf(stderr, "Iniciei processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
            }
            trocas_contexto++;
        }

        // Atualiza o tempo de vida do escalonador!!
        now = get_time();
        time_alive = (now - start)/1000000;   
    }

    now = get_time();
    time_alive = (now - start)/1000000;
    if (debug) {
        fprintf(stderr, "Trocas de contexto: %d\n", trocas_contexto);
        fprintf(stderr, "Tempo de vida escalodor: %f\n", time_alive);
    }   
    fprintf(output_f, "%d", trocas_contexto);
    return NULL;
}

void start_fcfs(FILE* file, int debug) {
    if (debug) {
        fprintf(stderr, "Iniciando o escalonador..\n");
    }
    fcfs_debug = debug;
    fifo_scheduling(file, debug);
}
