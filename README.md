#Shell e escalonadores de processos

Para gerar os executaveis **bccshell** e **./ep1**:
```$ make```

##Shell
Para abrir o shell:
```$ ./bccshell```
Features:
 - historico de comandos guardado (basta pressionar as setas UP e DOWN para navegar pelo historico)
 - estao implementadas as syscalls: ```mkdir```,```kill -9``` e ```ln -s```
 - para executar qualquer binario basta passar o nome do arquivo, referente ao diretorio atual. Exemplo:
 ```${usuario@diretorioAtual} /usr/bin/ls .``` lista os arquivos do diretorio atual. (So esta implementado para chamadas com no maximo 2 argumentos, sem contar o executavel)
 

por: Bruna Bazaluk e Diego Zurita
