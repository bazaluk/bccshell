#ifndef processo_h
#define processo_h 

#define MAX_PROC 100

typedef struct p {
	char* nome;
	int t0;
	int dt;
	int deadline;
    int enabled;
    double start;
    double remaining;
    double rr_time;
    int id;
	struct p* next;
    struct p* prev;
    
} Proc;

typedef struct
{
    int size;
    Proc* head;
} Fila;

typedef struct 
{
    int size;
    pthread_mutex_t mu[MAX_PROC];
    pthread_t tids[MAX_PROC];
} Lockers;

struct timespec ts;
struct timeval tv;

// Variaveis globais.
Fila* fila;
Lockers* lockers;

double get_time(); 
#endif