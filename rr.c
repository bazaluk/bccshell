#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <sched.h>

#include "processos.h"

#define QUANTUM 100 // milisegundos

int rr_debug = 0;

void* rr_process_job(void* p) {
    Proc* proc = (Proc*) p;
    double a, b;

    if (rr_debug) {
        int my_cpu = sched_getcpu();
        fprintf(stderr, "Processo: %s pegou a cpu %d\n", proc->nome, my_cpu);
    }

    a = 23;
    b = 12;
    while (1) {
        pthread_mutex_lock(&(lockers->mu[proc->id]));
        b = (a+b*b*a/b*a)*b*b*b*b;
        pthread_mutex_unlock(&(lockers->mu[proc->id]));
    }
    return NULL;
}

void* rr_scheduling(FILE* output_f, int debug) {
    // Controle de quanto tempo o escalonador esta no ar
    double start, now;
    unsigned int prev_is_over;
    unsigned int trocas_contexto = 0;

    double time_alive = 0; 
    start = get_time();
    if (debug) {
        fprintf(stderr, "Iniciei escalonador rr!!\n");
    }

    // Processo que está sendo executado agora.
    Proc* cur = NULL;
    Proc* cur_fila;
    Proc* next_proc;

    while(1) {
        prev_is_over = 0;
        next_proc = NULL;
        //
        // Atualiza o tempo de execução do processo atual
        // e mata caso  ja tenha executado tempo suficiente
        //
        if (cur) {

            double _now = get_time();
            cur->remaining -= (_now - cur->start)/1000000;//em segundos
            cur->rr_time -= (_now - cur->start)/1000; // milisegundos
            cur->start = _now;

            // Acabei de executar, tiro o processoa atual da fila
            // E falo que acabei, mas não mudo o curr ainda!!
            if (cur->remaining <= 0) {
                if (debug) {
                    fprintf(stderr, "---- Acabei de executar o processo %s pid: %d, time_alive: %f\n", cur->nome, cur->id, time_alive);
                }
                pthread_mutex_lock(&lockers->mu[cur->id]);
		        pthread_cancel(lockers->tids[cur->id]);

                // Atualiza o tempo do relogio exatamente
                now = get_time();
                time_alive = (now - start)/1000000;
                fprintf(output_f, "%s %f %f\n", cur->nome, time_alive, time_alive - cur->t0);

                //Tirar ele da fila!!
                // tiramos o next proc da fila
                if (cur->prev) {
                    cur->prev->next = cur->next;    
                }
                if (cur->next) {
                    cur->next->prev = cur->prev;
                }

                if (fila->head == cur) {
                    fila->head = fila->head->next;
                }

                fila->size--;
                prev_is_over = 1;
                if (debug) {
                    fprintf(stderr, "Tamanho da fila %d \n", fila->size);
                    fprintf(stderr, "\n");
                }
            }

            if (fila->size == 0) {
                if (debug) {
                    fprintf(stderr, "Acabei todos os processo, vitória!!!\n");
                }
                break;
            }
            // Rodou o tempo suficiente em quantum 
            // ou o processo anterior ja acabou!
            if (cur->rr_time <= 0 || prev_is_over) {
                // tem que ir para o proximo da fila!!!!!
                // Se acabou o tempo de execução dele
                // Ele não esta mais na fila
                cur_fila = cur->next;
                while (next_proc == NULL) {
                    // cheguei no fim da fila, volta pro começo!
                    if (cur_fila == NULL) {
                        cur_fila = fila->head;
                    }
                    else {
                        if (cur_fila->enabled) {
                            next_proc = cur_fila;
                        }
                        else {
                            if (time_alive >= cur_fila->t0) { 
                                cur_fila->enabled = 1;
                                next_proc = cur_fila;
                            }
                            else {
                                cur_fila = cur_fila->next;
                            }
                        }
                    }
                    // Atualiza o tempo de vida do escalonador!!
                    now = get_time();
                    time_alive = (now - start)/1000000;   
                }

                if (next_proc->id != cur->id) {
                    // Aqui tem que existir um next_proc, então
                    // Troca de contexto!!
                    // prev aqui extranhamente é o cur
                    // Se ele acabou, sua thread foi cancelada.
                    trocas_contexto++;
                    if (!prev_is_over) {
                        pthread_mutex_lock(&lockers->mu[cur->id]);
                    }
                    // Processo novo
                    cur = next_proc;
                    if (cur->id == -1) {
                        cur->id = lockers->size;
                        lockers->size++;
                        cur->start = get_time();
                        cur->rr_time = QUANTUM; // começa com 1 quantum
                        pthread_mutex_init(&lockers->mu[cur->id], NULL);
                        pthread_create(&lockers->tids[cur->id], NULL, rr_process_job, (void *)cur);
                        if (debug) {
                            fprintf(stderr, "Iniciei processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                        }
                    }
                    else {
                        cur->start = get_time();
                        cur->rr_time = QUANTUM; // começa com 1 quantum
                        pthread_mutex_unlock(&(lockers->mu[cur->id]));
                    }
                }
                else {
                    cur->start = get_time();
                    cur->rr_time = QUANTUM; // começa com 1 quantum     
                }
            }
        }
        // Não há processo rodando
        //
        // vai ficar só quando o escalonador é iniciado.
        // No if anterior, em nehum instante o cur fica nulo
        // novamente, por isso aqui só deve executar no inicio
        // do escalonador.
        else {
            // Não há proc sendo executado ainda..
            // ficar aqui até um ficar valido
            // pois a fila não esta vazia
            
            // começamos a busca pelo primeiro item da fila
            cur_fila = fila->head;
            while (next_proc == NULL) {
                // cheguei no fim da fila, volta pro começo!
                if (cur_fila == NULL) {
                    cur_fila = fila->head;
                }
                else {
                    if (cur_fila->enabled) {
                        next_proc = cur_fila;
                    }
                    else {
                        if (cur_fila->t0 <= time_alive) { 
                            cur_fila->enabled = 1;
                            next_proc = cur_fila;
                        }
                        else {
                            cur_fila = cur_fila->next;
                        }
                    }
                }
                // Atualiza o tempo de vida do escalonador!!
                now = get_time();
                time_alive = (now - start)/1000000;   
            }

            // Tenho um novo processo para rodar aqui
            // e não há um atual, então basta inicia-lo
            cur = next_proc;
            if (cur->id == -1) {
                cur->id = lockers->size;
                lockers->size++;
                cur->start = get_time();
                cur->rr_time = QUANTUM; // começa com 1 quantum
                pthread_mutex_init(&lockers->mu[cur->id], NULL);
                pthread_create(&lockers->tids[cur->id], NULL, rr_process_job, (void *)cur);
                if (debug) {
                    fprintf(stderr, "Iniciei processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                }
            }
            else {
                cur->start = get_time();
                cur->rr_time = QUANTUM; // começa com 1 quantum
                pthread_mutex_unlock(&lockers->mu[cur->id]);
                if (debug) {
                    fprintf(stderr, "Voltei para o processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                }
            }
        }

        // Atualiza o tempo de vida do escalonador!!
        now = get_time();
        time_alive = (now - start)/1000000;   
    }

    now = get_time();
    time_alive = (now - start)/1000000;   
    if (debug) {
        fprintf(stderr, "Trocas de contexto: %d\n", trocas_contexto);
        fprintf(stderr, "Tempo de vida escalodor: %f\n", time_alive);
    }
    fprintf(output_f, "%d", trocas_contexto);
    return NULL;
}

void start_rr(FILE* output_f, int d) {
    if (d) {
        fprintf(stderr, "Iniciando o escalonador..\n");
    }
    rr_debug = d;
    rr_scheduling(output_f, d);
}
