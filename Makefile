CC=gcc

all: bccshell ep1

bccshell: bccshell.c
	$(CC) -lreadline -Wall -o bccshell bccshell.c

ep1: ep1.c rr.o srtn.o fcfs.o processos.h escalonadores.h
	$(CC) -lpthread -Wall rr.o srtn.o fcfs.o ep1.c -o ep1

rr.o: rr.c processos.h
	$(CC) -lpthread -Wall -c rr.c

srtn.o: srtn.c processos.h
	$(CC) -lpthread -Wall -c srtn.c

fcfs.o: fcfs.c processos.h 
	$(CC) -lpthread -Wall -c fcfs.c

clean: 
	rm -f bccshell ep1 *.o
