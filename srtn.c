#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <sched.h>

#include "processos.h"

//MUITO CUIDADO
int strn_debug = 0;

void* srtn_process_job(void* p) {
    Proc* proc = (Proc*) p;
    double a, b;

    if (strn_debug) {
        int my_cpu = sched_getcpu();
        fprintf(stderr, "Processo: %s pegou a cpu %d\n", proc->nome, my_cpu);
    }

    a = 23;
    b =12;
    while (1) {
        pthread_mutex_lock(&lockers->mu[proc->id]);
        b = (a+b*b*a/b*a)*b*b*b*b;
        pthread_mutex_unlock(&lockers->mu[proc->id]);
    }
    return NULL;
}
void* strn_scheduling(int debug, FILE* output_f) {
    // Controle de quanto tempo o escalonador esta no ar
    double start, now;
    unsigned int trocas_contexto = 0;

    double time_alive = 0; 
    start = get_time();
    if (debug) {
        fprintf(stderr, "Iniciei escalonador strn!!\n");
    }

    // Processo que está sendo executado agora.
    Proc* cur = NULL;
    Proc* cur_fila;

    while(fila->size > 0) {
        //
        // Atualiza o tempo de execução do processo atual
        // e mata caso  ja tenha executado tempo suficiente
        //
        if (cur) {
            double _now = get_time();
            cur->remaining -= (_now/1000000 - cur->start/1000000);
            cur->start = _now;

            if (cur->remaining <= 0) {
                if (debug) {
                    fprintf(stderr, "Acabei de executar o processo %s pid: %d, time_alive: %f\n", cur->nome, cur->id, time_alive);
                }
                pthread_mutex_lock(&lockers->mu[cur->id]);
                pthread_cancel(lockers->tids[cur->id]);
                
                // Escrevo no arquivo de saida.
                now = get_time();
                time_alive = (now - start)/1000000;
                fprintf(output_f, "%s %f %f\n", cur->nome, time_alive, time_alive - cur->t0);
                cur = NULL;
            }
        }

        //
        // Encontra o proximo processo valido..
        //

        //Ponteiro auxiliar para passear na fila
        cur_fila = fila->head;
        // Armazena o próximo processo
        Proc* next_proc = cur;
        while (cur_fila != NULL) {
            if (cur_fila->enabled) {
                // senão houver nenhum processo sendo executado
                // ele é o proximo
                if (next_proc == NULL) {
                    next_proc = cur_fila;
                }
                // Ja há um processo rodando 
                else {
                    if (cur_fila->remaining < next_proc->remaining) {
                        next_proc = cur_fila;
                    }
                }
            }
            else {
                //Até aqui o processo esta desabilitado de ser executado..

                //Se ele chegou na fila, então pode ser habilitado para execução.
                if (cur_fila->t0 <= time_alive) {
                    cur_fila->enabled = 1;

                    if (next_proc == NULL) {
                        next_proc = cur_fila;
                    }
                    else {
                        if (cur_fila->remaining < next_proc->remaining) {
                            next_proc = cur_fila;
                        }
                    }
                }
            }
            cur_fila = cur_fila->next;
        }
        //
        //Coloca next_proc para executar!!
        //
        // Há um proximo processo para ser executado.
        if (next_proc != NULL) {
            // Não há processos rodando
            if (cur == NULL) {
                if (debug) {
                    fprintf(stderr, "Encontrei um novo processo para executar: %s \n", next_proc->nome);
                }
                // tiramos o next proc da fila
                if (next_proc->prev) {
                    next_proc->prev->next = next_proc->next;    
                }
                if (next_proc->next) {
                    next_proc->next->prev = next_proc->prev;
                }

                if (fila->head == next_proc) {
                    fila->head = fila->head->next;
                }

                fila->size--;

                cur = next_proc;
                // Caso ele ja tenha sido criado anteriormente, 
                // Somente destravo a sua thread
                if (cur->id != -1) {
                    cur->start = get_time();
                    pthread_mutex_unlock(&(lockers->mu[cur->id]));
                    if (debug) {
                        fprintf(stderr, "Voltei para o processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                    }
                }
                else {
                    cur->id = lockers->size;
                    lockers->size++;
                    cur->start = get_time();
                    pthread_create(&lockers->tids[cur->id], NULL, srtn_process_job, (void *)cur);
                    if (debug) {
                        fprintf(stderr, "Iniciei processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                    }
                }
            }
            //Há algum processo rodando
            else {
                // Houve troca de contexto
                if (next_proc->id != cur->id) {
                    trocas_contexto++;
                    // tiramos o next proc da fila
                    if (next_proc->prev) {
                        next_proc->prev->next = next_proc->next;    
                    }
                    if (next_proc->next) {
                        next_proc->next->prev = next_proc->prev;
                    }

                    if (fila->head == next_proc) {
                        fila->head = fila->head->next;
                    }

                    // colocamos o cur na fila novamente
                    if (fila->head != NULL) {
                        fila->head->prev = cur;
                    }
                    cur->next = fila->head;
                    cur->prev = NULL;
                    fila->head = cur;

                    // Para a thread atual!!
                    pthread_mutex_lock(&(lockers->mu[cur->id]));
                    if (debug) {
                        fprintf(stderr, "Parei processo %s (%d) rtime: %f, dt: %d. Tempo de vida: %f\n", cur->nome, cur->id, cur->remaining, cur->dt, time_alive);
                    }

                    cur = next_proc;
                    if (cur->id == -1) { // Processo novo
                        cur->id = lockers->size;
                        lockers->size++;
                        cur->start = get_time();
                        pthread_create(&lockers->tids[cur->id], NULL, srtn_process_job, (void *)cur);
                        if (debug) {
                            fprintf(stderr, "Iniciei processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                        }
                    }
                    else { // Processo ja foi criado em algum instante
                        cur->start = get_time();
                        pthread_mutex_unlock(&(lockers->mu[cur->id]));
                        if (debug) {
                            fprintf(stderr, "Voltei para o processo %s (%d) com tempo restante: %f. t0:%d - deadline:%d - time_alive:%f\n", cur->nome, cur->id, cur->remaining, cur->t0, cur->deadline, time_alive);
                        }
                    }
                }
            }
        }
        now = get_time();
        time_alive = (now - start)/1000000;
    }

    if (cur) {
        if (debug) {
            fprintf(stderr, "Ainda há processos sendo executados.\n");
        }

        while(1) {
            double _now = get_time();
            cur->remaining -= (_now - cur->start) / 1000000;
            cur->start = _now;
            if (cur->remaining <= 0) {
                if (debug) {
                    fprintf(stderr, "Acabei de executar o processo %s pid: %d, time_alive: %f\n", cur->nome, cur->id, time_alive);
                }
                pthread_mutex_lock(&lockers->mu[cur->id]);
                pthread_cancel(lockers->tids[cur->id]);
                cur = NULL;
                break;
            }
        }

        now = get_time();
        time_alive = (now - start)/1000000;
    }
    if (debug) {
        fprintf(stderr, "Trocas de contexto: %d\n", trocas_contexto);
        fprintf(stderr, "Tempo de vida: %f\n", time_alive);
    }
    fprintf(output_f, "%d", trocas_contexto);
    return NULL;
}

void start_strn(FILE* output_f, int d) {
    if (d) {
        fprintf(stderr, "Iniciando o escalonador..\n");
    }
    strn_debug = d;
    strn_scheduling(d, output_f);
}
