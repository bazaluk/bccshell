#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include "processos.h"
#include "escalonadores.h"

Fila* read_input(char *file){
    // EU ALTERI AQUI
    fila = (Fila*) malloc(sizeof(Fila));
    fila->size = 0;
    fila->head = NULL;

    Proc* cur = NULL;
    ssize_t read;
	size_t len = 0;
    FILE *f;
	char *line;
    f = fopen(file,"r");
    while((read = getline(&line, &len, f)) != -1){
        if (!fila->head) {
            void* r = malloc(sizeof(Proc));
            if (r == NULL) {
                printf("Pânico para alocar memoria!\n");
            }
            cur = (Proc*) r;
            cur->nome = (char *) malloc(sizeof(char));
            strcpy(cur->nome, strtok(line," "));
            cur->t0 = atoi(strtok(NULL," "));
            cur->dt = atoi(strtok(NULL," "));
            cur->deadline = atoi(strtok(NULL," \n"));
            cur->enabled = 0;
            cur->id = -1;
            cur->start = 0.0;
            cur->remaining = (double) cur->dt;
            cur->rr_time = 0.0;
            cur->next = NULL;
            cur->prev = NULL;

            fila->head = cur;
            fila->size++;
        }
        else {
            Proc* temp = (Proc*) malloc(sizeof(Proc));
            temp->nome = (char *) malloc(sizeof(char));
            strcpy(temp->nome, strtok(line," "));
            temp->t0 = atoi(strtok(NULL," "));
            temp->dt = atoi(strtok(NULL," "));
            temp->deadline = atoi(strtok(NULL," \n"));
            temp->enabled = 0;
            temp->id = -1;
            temp->start = 0.0;
            temp->remaining = (double) temp->dt;
            temp->rr_time = 0.0;
            temp->next = NULL;
            temp->prev = NULL;
    
            temp->prev = cur;
            cur->next = temp;
            cur = temp;
            fila->size++;
        }
    }
    return fila;
}

double get_time() {
    gettimeofday(&tv,NULL);
    return 1000000 * tv.tv_sec + tv.tv_usec;
}


/*parametros q recebe:
 * numero do escalonador
 * arquivo trace*
 * arquivo de saida
 */

int main(int argc, char *argv[])
{
	//char *file;
	//1 FCFS
	//2 SRTN
	//3 RoundRobin
	//  argv-> [escalonador,trace,arquivo de saida] 
	char escalonador = argv[1][0];
	fila = read_input(argv[2]);

    // start thread lockers
    void* v = malloc(sizeof(Lockers));
    if (v == NULL) {
        printf("Pânico para alocar lockers\n");
    }

    lockers = (Lockers*) v;
    lockers->size = 0; // Proxima posição valida do vetor de mutex

	FILE *output_f = fopen(argv[3],"w");
	int debug = 0;
	if(argc == 5){
		if(argv[4][0]=='d') debug = 1;
	}
	//printf("debug %d \n", argc);
	if(escalonador == '1'){
		start_fcfs(output_f,debug);	
	}
	else if(escalonador == '2'){
		start_strn(output_f,debug);	
	}
	else if(escalonador == '3'){
		start_rr(output_f,debug);	
	}
	else printf("Número inválido, as opções são:\n"
				"1 -> First Come First Served\n"
				"2 -> Shortest Remaining Time Next\n"
				"3 -> Round Robin\n");

    //Libera memoria
    free(lockers);
    Proc* cur = fila->head;
    Proc* temp;
    while(cur != NULL) {
        temp = cur->next;
        free(cur->next);
        free(cur);    
        cur = temp;    
    }
    fclose(output_f);
	return 0;
}
